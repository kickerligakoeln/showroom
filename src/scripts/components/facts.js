var Facts = function () {
};


/**
 *
 */
Facts.prototype.init = function (duration) {

    if(duration === undefined){
        duration = 7000;
    }

    var elems = jQuery('[data-slider="slick"]');
    if (elems.length > 0) {
        this.initSlick(elems, {
            autoplay: true,
            autoplaySpeed: duration,
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        })
    }
};


/**
 *
 * @param $elem
 * @param options
 */
Facts.prototype.initSlick = function ($elem, options) {
    $elem.slick(options);
};