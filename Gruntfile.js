module.exports = function (grunt) {

    require('time-grunt')(grunt);

    let conf = {
        cwd: 'src/',
        dest: 'dist/',

        cssCwd: 'src/scss/',
        cssDest: 'dist/css/',

        vendorCwd: 'node_modules/',
        jsCwd: 'src/scripts/',
        jsCompile: 'src/scripts/_tmp/',
        jsDest: 'dist/js/',

        imgCwd: 'src/images',
        imgDest: 'dist/images',

        fontCwd: 'src/fonts',
        fontDest: 'dist/fonts',

        tmplCwd: 'src/templates/',
        tmplDest: 'dist/templates/',

        viewsCwd: 'src/views/',
        viewsDest: 'dist/',
    };

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        /**
         * Replace variables in files depending on environment
         */
        replace: {
            views: {
                options: {
                    patterns: [
                        {
                            // include timestamp in template (e.g. @@timestamp)
                            match: 'timestamp',
                            replacement: '<%= new Date() %>'
                        },
                        {
                            // include templates (e.g. @@inc=folder/filename)
                            match: /(@@inc=)\S+/g,
                            replacement: function (match, offset, string, source, target) {
                                var templateName = match.replace(/@@inc=/g, '');
                                return grunt.file.read(conf.viewsCwd + "partials/" + templateName + ".html");
                            }
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        cwd: conf.viewsCwd,
                        src: ['**', '!partials/**'],
                        dest: conf.viewsDest
                    }
                ]
            },
            vars: {
                options: {
                    patterns: [
                        {
                            // include timestamp to avoid caching in template (e.g. @@cache)
                            match: 'cache',
                            replacement: '<%= Date.now() %>'
                        },
                        {
                            // set environment vars in template
                            match: /(@@\w+@@)/g,
                            replacement: function (match) {
                                return conf.env[match.replace(/@@/g, '')];
                            }
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        cwd: conf.viewsDest,
                        src: [
                            '**/**.html',
                            '**.html',
                            '!partials/**'
                        ],
                        dest: conf.viewsDest
                    },
                    {
                        expand: true,
                        cwd: conf.jsCwd,
                        src: ['**.js'],
                        dest: conf.jsDest
                    }
                ]
            }
        },


        /**
         * Minify files with UglifyJS and move to dist
         * https://github.com/gruntjs/grunt-contrib-uglify
         */
        uglify: {
            libraries: {
                files: [{
                    src: [
                        conf.vendorCwd + "jquery/dist/jquery.js",
                        conf.vendorCwd + "slick-carousel/slick/slick.js"
                    ],
                    dest: conf.jsDest + "libraries.min.js"
                }]
            },
            app: {
                files: [
                    {
                        src: [
                            conf.jsCwd + '**/*.js'
                        ],
                        dest: conf.jsDest + 'app.min.js'
                    }
                ]
            }
        },


        /**
         * Compile Sass
         */
        sass: {
            dist: {
                options: {
                    outFile: null,
                    sourceMap: false,
                    sourceMapContents: true,
                    sourceMapRoot: '../../',
                    outputStyle: 'compressed'
                },

                files: [{
                    expand: true,
                    cwd: conf.cssCwd,
                    src: [
                        '*.scss'
                    ],
                    dest: conf.cssDest,
                    ext: '.min.css'
                }]
            }
        },


        postcss: {
            options: {
                map: {
                    inline: false,
                    annotation: conf.cssDest
                },
                processors: [
                    require('autoprefixer')({browsers: 'last 2 versions, safari 8'}), // add vendor prefixes
                    require('postcss-discard-comments')({removeAll: true}) // remove comments
                ]
            },
            dist: {
                src: conf.cssDest + '*.css'
            }
        },


        /**
         * Copy Files & Dependencies
         */
        copy: {
            images: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: conf.imgCwd,
                        src: '**',
                        dest: conf.imgDest
                    }
                ]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: conf.fontCwd,
                        src: ['**/*.otf', '**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff', '**/*.woff2'],
                        dest: conf.fontDest
                    }
                ]
            },
            templates: {
                files: [
                    {
                        expand: true,
                        flatten: false,
                        cwd: conf.tmplCwd,
                        src: ['**/*.html'],
                        dest: conf.tmplDest
                    }
                ]
            }
        },


        /**
         * Watch Tasks
         */
        watch: {
            css: {
                files: [
                    conf.cssCwd + '**/*.scss'
                ],
                tasks: ['sass'],
                options: {
                    spawn: false
                }
            },
            scripts: {
                files: [
                    conf.jsCwd + '**/*.js'
                ],
                tasks: ['uglify'],
                options: {
                    spawn: false
                }
            },
            views: {
                files: [
                    conf.viewsCwd + '/**'
                ],
                tasks: ['replace'],
                options: {
                    spawn: false
                }
            },
            templates: {
                files: [
                    conf.tmplCwd + "**/*.html"
                ],
                tasks: ['copy:templates'],
                options: {
                    spawn: false
                }
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-replace');

    grunt.registerTask("set_dev_options", "Set Config variables for dev tasks", function () {
        grunt.config.set("sass.dist.options.sourceMap", true);
    });

    // Define Task(s)
    grunt.registerTask('default', ['sass', 'postcss', 'uglify', 'copy', 'replace']);
    grunt.registerTask('dev', ['set_dev_options', 'default', 'watch']);
};